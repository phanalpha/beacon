BEACON - Webhook stopwatch
===

CREATE
---

`POST` /alarms

### Form Parameters

* `retain` retain the alarm after fired (before release), in seconds; optional, &le; 1209600 (2 weeks)
* `refno` id, the
* `expire_at` RFC3339
* `method` http verb (webhook); `DELETE` `GET` `HEAD` `PATCH` `POST` `PUT`
* `url` url (webhook)
* `header` header (webhook); optional
* `body` body (webhook); optional

### Example

```sh
$ curl -d 'refno=rn3iam6fQImBDZ_nhNDgrg&expire_at=2018-07-29T08:00:00Z&method=POST&url=https://still-refuge-27761.herokuapp.com/1apttem1&body=hello.' http://127.0.0.1:7963/alarms
```

```json
{
  "RefNo": "rn3iam6fQImBDZ_nhNDgrg",
  "Retain": 120,
  "ExpireAt": "2018-07-29T08:00:00Z",
  "Method": "POST",
  "URL": "https://still-refuge-27761.herokuapp.com/1apttem1",
  "Body": "hello."
}
```

UPDATE
---

Reset alarm before released.

`PATCH` /alarms/`:refno`

### Form Parameters (optional)

* `retain` retain the alarm after fired, in seconds; <= 1209600
* `expire_at` RFC3339
* `method` http verb (webhook); `DELETE` `GET` `HEAD` `PATCH` `POST` `PUT`
* `url` url (webhook)
* `header` header (webhook)
* `body` body (webhook)

### Example

```sh
$ curl -X PATCH -d 'retain=120&expire_at=2018-08-07T10:40:00Z&body=hello' http://127.0.0.1:7963/alarms/rn3iam6fQImBDZ_nhNDgrg
```

```json
{
    "Retain": 120,
    "ExpireAt": "2018-08-07T10:40:00Z",
    "Method": "GET",
    "URL": "https://still-refuge-27761.herokuapp.com/15w57g81",
    "Body": "hello"
}
```

REMOVE
---

Remove ticker before release.

`DELETE` /alarms/`:refno`

### Example

```sh
$ curl -X DELETE http://127.0.0.1:7963/alarms/rn3iam6fQImBDZ_nhNDgrg; echo
```

INSPECT
---

`GET` /alarms

### Example

```sh
$ curl http://127.0.0.1:7963/alarms
```

```json
{
    "UK3RdudQqJaJoyh6JzXMjp": {
        "Retain": 120,
        "ExpireAt": "2018-08-07T10:40:00Z",
        "Method": "GET",
        "URL": "https://still-refuge-27761.herokuapp.com/15w57g81",
        "Body": "hello"
    }
}
```

WEBSOCKET
---

### Message

```typescript
interface Event {
    tag: 'op'|'up'|'rm'|'ed';
    expire_at: string; // RFC3339
}
```

### Tag

* `op` (websocket) connection open
* `up` update
* `rm` remove
* `ed` fire; or if already fired on open

### Example

```sh
$ wscat -c http://127.0.0.1:7963/alarms/rn3iam6fQImBDZ_nhNDgrg
```

```
connected (press CTRL+C to quit)
< {"tag":"op","expire_at":"2018-07-29T08:00:00Z"}

< {"tag":"up","expire_at":"2018-07-29T07:00:00Z"}

< {"tag":"rm","expire_at":"2018-07-29T14:45:02.85056747+08:00"}

disconnected (code: 1006)
```

```
connected (press CTRL+C to quit)
< {"tag":"op","expire_at":"2018-07-29T08:00:00Z"}

< {"tag":"up","expire_at":"2018-07-29T07:00:00Z"}

< {"tag":"ed","expire_at":"2018-07-29T15:00:00.000128246+08:00"}

disconnected (code: 1006)
```

Webhook
---

![RequestBin](requestbin_t7jx81.png)

Configuration
---

`GET` /_conf
### Example

```sh
$ curl -s http://root:0AmabZTxTpNm31pr8z5Nbt2v@127.0.0.1:7963/_conf
```

```json
{
  "Database": "mongodb://******",
  "Address": "127.0.0.1:7963",
  "Retain": 120,
  "Timeout": 120
}
```
