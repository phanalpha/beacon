package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"log"
	"net/http"
	"os"
	"os/user"
	"path"
	"reflect"
	"sort"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/globalsign/mgo"
	"github.com/gorilla/websocket"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend"
	"github.com/heetch/confita/backend/env"
	"github.com/heetch/confita/backend/file"
	"github.com/heetch/confita/backend/flags"
	"gopkg.in/go-playground/validator.v8"
)

var verbs = []string{
	"DELETE",
	"GET",
	"HEAD",
	"PATCH",
	"POST",
	"PUT",
}

func verb(_ *validator.Validate, _ reflect.Value, _ reflect.Value, field reflect.Value, _ reflect.Type, _ reflect.Kind, _ string) bool {
	i := sort.SearchStrings(verbs, field.String())

	return i < len(verbs) && verbs[i] == field.String()
}

type Config struct {
	Database string `config:"database,required"`
	Address  string `config:"address"`
	Retain   int    `config:"retain"`
	Timeout  int    `config:"timeout"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
}

func main() {
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("verb", verb)
	}

	curr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	backends := []backend.Backend{env.NewBackend()}
	for _, name := range []string{
		"beacon.yaml",
		"beacon.json",
		path.Join(curr.HomeDir, ".beacon", "config.yaml"),
		path.Join(curr.HomeDir, ".beacon", "config.json"),
	} {
		if _, err := os.Stat(name); err != nil {
			continue
		}

		backends = append(backends, file.NewBackend(name))
	}
	backends = append(backends, flags.NewBackend())

	config := Config{
		Address: "127.0.0.1:7963",
		Retain:  7200,
		Timeout: 120,
	}
	if err := confita.NewLoader(backends...).Load(context.Background(), &config); err != nil {
		log.Fatal(err)
	}

	s := make([]byte, 18)
	if _, err := rand.Read(s); err != nil {
		log.Fatal(err)
	}
	secret := base64.RawURLEncoding.EncodeToString(s)
	log.Print(secret)

	session, err := mgo.Dial(config.Database)
	if err != nil {
		log.Fatal(err)
	}

	r := NewQueueRunner(session, time.Duration(config.Timeout)*time.Second)

	g := gin.Default()
	g.GET("/_conf", gin.BasicAuth(gin.Accounts{"root": secret}), func(c *gin.Context) {
		c.JSON(http.StatusOK, config)
	})

	alarms := g.Group("/alarms")
	alarms.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, r.Recap())
	})
	alarms.POST("", func(c *gin.Context) {
		ticker := Ticker{Alarm: &Alarm{Retain: config.Retain}}
		if err := c.Bind(&ticker); err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, err)

			return
		}

		if err := r.Insert(&ticker); err != nil {
			log.Println(err)
			c.JSON(http.StatusConflict, err)

			return
		}

		c.JSON(http.StatusCreated, ticker)
	})
	alarms.GET("/:refno", func(c *gin.Context) {
		conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
		if err != nil {
			log.Fatal(err)
		}

		vch, cch := rock(conn)
		refNo := c.Param("refno")
		if !r.Subscribe(refNo, vch) {
			close(vch)
		}

		<-cch
		if r.Unsubscribe(refNo, vch) {
			close(vch)
		}
	})
	alarms.PATCH("/:refno", func(c *gin.Context) {
		a, ok := r.Recap()[c.Param("refno")]
		if !ok {
			c.JSON(http.StatusNotFound, err)

			return
		}

		a2 := a.Replicate()
		if err := c.Bind(&a2); err != nil {
			c.JSON(http.StatusBadRequest, err)

			return
		}
		if err := r.Update(c.Param("refno"), &a2); err != nil {
			c.JSON(http.StatusNotFound, err)

			return
		}

		c.JSON(http.StatusOK, a2)
	})
	alarms.DELETE("/:refno", func(c *gin.Context) {
		if err := r.Remove(c.Param("refno")); err != nil {
			c.JSON(http.StatusNotFound, err)

			return
		}

		c.Status(http.StatusNoContent)
	})

	g.Run(config.Address)
}

func rock(conn *websocket.Conn) (chan Event, chan interface{}) {
	vch := make(chan Event, 5)
	cch := make(chan interface{})

	go func() {
		for {
			if _, _, err := conn.NextReader(); err != nil {
				break
			}
		}

		conn.Close()
		close(cch)
	}()

	go func() {
		for {
			ev, ok := <-vch
			if !ok {
				break
			}
			if err := conn.WriteJSON(ev); err != nil {
				break
			}
		}

		conn.Close()
	}()

	return vch, cch
}
