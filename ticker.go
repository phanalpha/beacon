package main

import (
	"time"
)

var maximumTime = time.Unix(1<<63-62135596801, 999999999)

type Tag string

const (
	OP Tag = "op"
	UP Tag = "up"
	ED Tag = "ed"
	RM Tag = "rm"
)

type Event struct {
	Tag      `json:"tag"`
	ExpireAt time.Time `json:"expire_at"`
}

type Phase int

const (
	FREE   Phase = iota
	FIRED        // just fired
	FAILED       // webhook error
	DONE
)

type Alarm struct {
	Retain   int       `form:"retain" binding:"max=1209600"`
	ExpireAt time.Time `bson:"expire_at" form:"expire_at" binding:"required" time_format:"2006-01-02T15:04:05Z07:00"`
	WebHook
}

func (a *Alarm) Replicate() Alarm {
	return Alarm{
		a.Retain,
		time.Unix(a.ExpireAt.Unix(), a.ExpireAt.UnixNano()),
		a.WebHook.Replicate(),
	}
}

type Ticker struct {
	RefNo string `bson:"_id" form:"refno" binding:"required"`
	*Alarm
	FiredAt       *time.Time `bson:"fire_at,omitempty" form:"-" json:"-"`
	StatusCode    *int       `bson:"status_code,omitempty" form:"-" json:"-"`
	hot           bool
	fib           Fib
	subscriptions []chan<- Event
}

func (t *Ticker) Phase() Phase {
	switch {
	case t.FiredAt == nil:
		return FREE

	case t.hot:
		return FIRED

	case t.fib.Get() > 0:
		return FAILED

	default:
		return DONE
	}
}

func (t *Ticker) Advise() time.Time {
	switch t.Phase() {
	case FREE:
		return t.ExpireAt

	case FIRED:
		return maximumTime

	case FAILED:
		return t.ExpireAt.Add(time.Duration(t.fib.Get()) * time.Second)

	default:
		return t.ExpireAt.Add(time.Duration(t.Retain) * time.Second)
	}
}

func (t *Ticker) Subscribe(subscription chan<- Event) bool {
	if !dispatch(subscription, Event{OP, t.ExpireAt}) {
		return false
	}
	if t.FiredAt != nil && !dispatch(subscription, Event{ED, *t.FiredAt}) {
		return false
	}

	t.subscriptions = append(t.subscriptions, subscription)
	return true
}

func (t *Ticker) Unsubscribe(subscription chan<- Event) bool {
	for i, ch := range t.subscriptions {
		if ch != subscription {
			continue
		}

		n := len(t.subscriptions) - 1
		t.subscriptions[i], t.subscriptions[n] = t.subscriptions[n], nil
		t.subscriptions = t.subscriptions[:n]

		return true
	}

	return false
}

func (t *Ticker) Reset(a *Alarm) {
	t.hot = false
	t.fib.Reset()

	t.FiredAt = nil
	t.Alarm = a
	t.dispatch(Event{"up", t.ExpireAt})
}

func (t *Ticker) Fire(firedAt time.Time) {
	t.hot = true

	if t.FiredAt == nil {
		t.fib.Reset()

		t.FiredAt = &firedAt
		t.dispatch(Event{ED, firedAt})
	}
}

func (t *Ticker) Cooldown(statusCode int, err error) {
	if !t.hot {
		// ignore cooldown if not fired (reset)
		return
	}

	t.hot = false
	t.StatusCode = &statusCode
	if err != nil {
		t.fib.Next()
	} else {
		t.fib.Reset()
	}
}

func (t *Ticker) Close(removedAt *time.Time) {
	if removedAt != nil {
		t.dispatch(Event{RM, *removedAt})
	}
	for _, ch := range t.subscriptions {
		close(ch)
	}

	t.subscriptions = nil
}

func (t *Ticker) dispatch(e Event) {
	i, n := 0, len(t.subscriptions)
	for i < n {
		if dispatch(t.subscriptions[i], e) {
			i++
			continue
		}

		n--
		t.subscriptions[i] = t.subscriptions[n]
		t.subscriptions[n] = nil
		t.subscriptions = t.subscriptions[:n]
	}
}

func dispatch(ch chan<- Event, e Event) bool {
	select {
	case ch <- e:
		return true

	default:
		close(ch)
	}

	return false
}

type Fib struct {
	i, j int
}

func (f *Fib) Reset() {
	f.i, f.j = 0, 1
}

func (f *Fib) Next() {
	f.i, f.j = f.j, f.j+f.i
}

func (f *Fib) Get() int {
	return f.i
}
