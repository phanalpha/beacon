package main

import (
	"context"
	"log"
	"sort"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type command interface {
	Run(r *QueueRunner) (pure bool)
}

type QueueRunner struct {
	collection *mgo.Collection
	timeout    time.Duration
	queue      []*Ticker
	ticker     *time.Ticker
	commands   chan command
}

func NewQueueRunner(session *mgo.Session, timeout time.Duration) *QueueRunner {
	runner := &QueueRunner{
		session.DB("").C("ticker"),
		timeout,
		nil,
		time.NewTicker(1),
		make(chan command),
	}
	if err := runner.collection.Find(bson.M{"fire_at": nil}).All(&runner.queue); err != nil {
		log.Fatal(err)
	}

	return runner.start()
}

func (r *QueueRunner) start() *QueueRunner {
	go func() {
		for {
			var ch <-chan time.Time
			if r.ticker != nil {
				ch = r.ticker.C
			}

			select {
			case <-ch:
			case cmd := <-r.commands:
				if cmd.Run(r) {
					continue
				}
			}

			if r.ticker != nil {
				r.ticker.Stop()
				r.ticker = nil
			}

			sort.Slice(r.queue, func(i, j int) bool {
				return r.queue[i].Advise().Before(r.queue[j].Advise())
			})
			r.tick()
		}
	}()

	return r
}

func (r *QueueRunner) tick() {
	for i, ticker := range r.queue {
		cd := time.Until(ticker.Advise())
		if cd > 0 {
			r.ticker = time.NewTicker(cd)

			break
		}

		switch ticker.Phase() {
		case FREE, FAILED:
			ticker.Fire(time.Now())

			ctx, _ := context.WithTimeout(context.Background(), r.timeout)
			go func(h *WebHook) {
				statusCode := -1
				res, err := h.Fire(ctx)
				if res != nil {
					statusCode = res.StatusCode
				}

				r.commands <- cooldown{ticker.RefNo, statusCode, err}
			}(&ticker.WebHook)

		case FIRED:
			panic("hot")

		default:
			ticker.Close(nil)
			r.queue[i] = nil

			if err := r.collection.Update(bson.M{"_id": ticker.RefNo}, ticker); err != nil {
				log.Fatal(err)
			}
		}
	}

	i, n := 0, len(r.queue)
	for i < n {
		if r.queue[i] != nil {
			i++
			continue
		}

		n--
		r.queue[i] = r.queue[n]
		r.queue[n] = nil
		r.queue = r.queue[:n]
	}
}

type insert struct {
	ticker *Ticker
	done   chan error
}

func (cmd insert) Run(r *QueueRunner) bool {
	r.queue = append(r.queue, cmd.ticker)

	cmd.done <- nil
	return false
}

type update struct {
	refNo string
	alarm *Alarm
	done  chan error
}

func (cmd update) Run(r *QueueRunner) bool {
	for _, ticker := range r.queue {
		if ticker.RefNo != cmd.refNo {
			continue
		}

		ticker.Reset(cmd.alarm)
		if err := r.collection.Update(bson.M{"_id": ticker.RefNo}, ticker); err != nil {
			log.Fatal(err)
		}

		break
	}

	cmd.done <- nil
	return false
}

type remove struct {
	refNo string
	done  chan error
}

func (cmd remove) Run(r *QueueRunner) bool {
	for i, ticker := range r.queue {
		if ticker.RefNo != cmd.refNo {
			continue
		}

		n := len(r.queue) - 1
		r.queue[i], r.queue[n] = r.queue[n], nil
		r.queue = r.queue[:n]

		hour := time.Now()
		ticker.Close(&hour)
		if err := r.collection.Remove(bson.M{"_id": cmd.refNo}); err != nil {
			log.Fatal(err)
		}
		break
	}

	cmd.done <- nil
	return false
}

type cooldown struct {
	refNo      string
	statusCode int
	err        error
}

func (cmd cooldown) Run(r *QueueRunner) bool {
	for _, ticker := range r.queue {
		if ticker.RefNo != cmd.refNo {
			continue
		}

		ticker.Cooldown(cmd.statusCode, cmd.err)
		break
	}

	return false
}

type recap struct {
	done chan map[string]*Alarm
}

func (cmd recap) Run(r *QueueRunner) bool {
	alarms := make(map[string]*Alarm)
	for _, ticker := range r.queue {
		alarms[ticker.RefNo] = ticker.Alarm
	}

	cmd.done <- alarms
	return true
}

type subscribe struct {
	flag  bool
	refNo string
	ch    chan<- Event
	ok    chan bool
}

func (cmd subscribe) Run(r *QueueRunner) bool {
	ok := false
	for _, ticker := range r.queue {
		if ticker.RefNo != cmd.refNo {
			continue
		}

		if cmd.flag {
			ok = ticker.Subscribe(cmd.ch)
		} else {
			ok = ticker.Unsubscribe(cmd.ch)
		}
		break
	}

	cmd.ok <- ok
	return true
}

func (r *QueueRunner) Insert(ticker *Ticker) error {
	if err := r.collection.Insert(ticker); err != nil {
		return err
	}

	done := make(chan error)
	r.commands <- insert{ticker, done}
	if err := <-done; err != nil {
		log.Fatal(err)
	}

	return nil
}

func (r *QueueRunner) Update(refNo string, a *Alarm) error {
	done := make(chan error)
	r.commands <- update{refNo, a, done}

	return <-done
}

func (r *QueueRunner) Remove(refNo string) error {
	done := make(chan error)
	r.commands <- remove{refNo, done}

	return <-done
}

func (r *QueueRunner) Recap() map[string]*Alarm {
	done := make(chan map[string]*Alarm)
	r.commands <- recap{done}

	return <-done
}

func (r *QueueRunner) Subscribe(refNo string, ch chan<- Event) bool {
	ok := make(chan bool)
	r.commands <- subscribe{true, refNo, ch, ok}

	return <-ok
}

func (r *QueueRunner) Unsubscribe(refNo string, ch chan<- Event) bool {
	ok := make(chan bool)
	r.commands <- subscribe{false, refNo, ch, ok}

	return <-ok
}
