package main

import (
	"bytes"
	"context"
	"errors"
	"net/http"
)

type WebHook struct {
	Method string      `bson:"method" form:"method" binding:"required,verb"`
	URL    string      `bson:"url" form:"url" binding:"required,url"`
	Header http.Header `bson:"header,omitempty" form:"header" json:",omitempty"`
	Body   string      `bson:"body,omitempty" form:"body" json:",omitempty"`
}

func (h *WebHook) Replicate() WebHook {
	h2 := *h
	h2.Header = make(http.Header, len(h.Header))
	for k, vv := range h.Header {
		vv2 := make([]string, len(vv))
		copy(vv2, vv)
		h2.Header[k] = vv2
	}

	return h2
}

func (h *WebHook) Fire(ctx context.Context) (*http.Response, error) {
	req, err := http.NewRequest(h.Method, h.URL, bytes.NewReader([]byte(h.Body)))
	if err != nil {
		return nil, err
	}

	for name, values := range h.Header {
		for _, value := range values {
			req.Header.Add(name, value)
		}
	}

	res, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}

	if http.StatusOK <= res.StatusCode && res.StatusCode < http.StatusMultipleChoices {
		return res, nil
	}

	return res, errors.New(res.Status)
}
